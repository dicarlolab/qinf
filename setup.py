from setuptools import setup

setup(
    name='qinf',
    version='0.0.1-dev0',
    packages=['qinf'],
    package_data={
        'qinf': ['config.json'],
        'qinf': ['templates/*.json']
    },
    url='',
    license='BSD 2-clause',
    author='Viacheslav Ostroukh',
    author_email='V.Ostroukh@tudelft.nl',
    description='',
    install_requires=list(open('requirements.txt').read().strip().split('\n')),
)
