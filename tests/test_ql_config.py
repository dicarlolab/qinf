import numpy as np
from qinf.core import Program
from qinf.ql_config import rotations_to_mw_config, ccl_microwave_instruction,\
    named_instructions_from_rotations, kraus_operators_from_rotations
from qinf import ql_config


class TestQLConfig:

    def test_qlconfig_valid(self):
        """
        Tests if a generated config is valid by running a simple algorithm.
        """

        allxy = [['rx180', 'rx180'],
                 ['ry180', 'ry180'],
                 ['rx180', 'ry180']]

        program = Program("allXY")
        assert program.qubit_number('QL') == 0
        assert program.qubit_number('QR') == 2

        for i, sequence in enumerate(allxy):
            kernel = program.new_kernel('allxy{}'.format(i))
            kernel.prepz('QL', 'QR')
            kernel.gate(sequence[0], ['QL'])
            kernel.gate(sequence[1], ['QR'])
            kernel.measure('QL', 'QR')
        program.compile()
        assert program.name == 'allXY'

    def test_rotations_to_mw_config(self):
        rotations = [(0, 180), (0, 90), (90, 180)]
        mw_config = rotations_to_mw_config(rotations)

        expected_mw_lutmap = {
            0: {"name": "I", "theta": 0, "phi": 0, "type": "ge"},
            1: {"name": "rot_1", "theta": 180, "phi": 0, "type": "ge"},
            2: {"name": "rot_2", "theta": 90, "phi": 0, "type": "ge"},
            3: {"name": "rot_3", "theta": 180, "phi": 90, "type": "ge"}, }
        for i in range(3):
            assert mw_config[i] == expected_mw_lutmap[i]

    def test_ccl_microwave_instruction(self):
        cw = 4
        instr = ccl_microwave_instruction(
            codeword=cw, target_qubit=2,
            kraus_operator=None, mw_pulse_duration=20)
        exp_instr = {
            "duration": 20,
            "latency": 0,
            "qubits": ["q2"],
            "matrix": [[0.0, 1.0], [1.0, 0.0], [1.0, 0.0], [0.0, 0.0]],
            "kraus_repr": None,
            "disable_optimization": False,
            "type": "mw",
            "cc_light_instr_type": "single_qubit_gate",
            "cc_light_instr": "cw_{:02}".format(cw),
            "cc_light_codeword": cw,
            "cc_light_opcode": 8+cw}
        assert instr == exp_instr

    def test_named_instructions_from_rotations(self):
        rotations = [(0, 180.0), (0, 90.4), (90, 270), (90, -90)]
        instrs = named_instructions_from_rotations(rotations, target_qubit=2)

        assert instrs[0] == 'rot_0_180 q2'
        assert instrs[1] == 'rot_0_90.4 q2'
        # assert instrs[2] == 'rot_90_m90 q2' # FIXME
        assert instrs[3] == 'rot_90_m90 q2'

    def test_kraus_operators_from_rotations(self):
        rotations = [(0, 180.0), (0, 90.), (90, 180), (90, -90)]
        kraus_operators = kraus_operators_from_rotations(rotations)
        sqrt2i = 2**(-0.5)
        np.testing.assert_almost_equal(
            kraus_operators[0],
            [np.array([[0., -1j], [-1j, 0]])])
        np.testing.assert_almost_equal(
            kraus_operators[1],
            [np.array([[sqrt2i, -1j*sqrt2i], [-1j*sqrt2i, sqrt2i]])])
        np.testing.assert_almost_equal(
            kraus_operators[2],
            [np.array([[0., -1], [1, 0]])])
        np.testing.assert_almost_equal(
            kraus_operators[3],
            [np.array([[sqrt2i, sqrt2i], [-sqrt2i, sqrt2i]])])

    def test_generate_resource_description(self):
        resources = ql_config.generate_resource_description()

        assert resources['qubits'] == {"count": 7}
        assert {"qubits", "qwgs", "meas_units", "edges"} == \
            set(resources.keys())

    def test_generate_topology_description(self):
        topology = ql_config.generate_topology_description()

        assert {"x_size", "y_size", "qubits", "edges"} ==\
            set(topology.keys())
