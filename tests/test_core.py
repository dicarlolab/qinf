import pytest
import numpy as np
from qinf.core import Program, HardwareConstraintsError, InvalidKernelName


# noinspection PyProtectedMember
class TestProgram:
    def test_basic_allxy_program(self):
        allxy = [['i', 'i'],
                 ['rx180', 'rx180'],
                 ['ry180', 'ry180'],
                 ['rx180', 'ry180'], ]

        program = Program("allXY")
        for i, sequence in enumerate(allxy):
            kernel = program.new_kernel('allxy{}'.format(i))
            kernel.prepz('QL', 'QR')
            kernel.gate(sequence[0], ['QL'])
            kernel.gate(sequence[1], ['QL'])
            kernel.measure('QL', 'QR')
        program.compile()
        assert(program.name == 'allXY')

        assert 'allXY.qisa' in program.qisa_fp
        assert 'allXY.qasm' in program.qasm_fp
        assert 'ccl_config.json' in program.ccl_config_fp
        assert 'mw_config.json' in program.mw_config_fp

    def test_basic_two_qubit_program(self):
        allxy = [['i', 'i'],
                 ['rx180', 'rx180'],
                 ['ry180', 'ry180'],
                 ['rx180', 'ry180'], ]

        program = Program("basic_two_qubit")
        for i, sequence in enumerate(allxy):
            kernel = program.new_kernel('basic_two_qubit{}'.format(i))
            kernel.prepz('QL', 'QR')
            kernel.gate(sequence[0], ['QL'])
            kernel.gate(sequence[1], ['QR'])
            kernel.cz('QR', 'QL')
            kernel.rotate('QL', 90, 90)
            kernel.measure('QL', 'QR')
        program.compile()
        assert program.name == 'basic_two_qubit'

    def test_cz_symmetric_program(self):
        program = Program("symmetric_CZ")

        kernel = program.new_kernel('first_kern')
        kernel.prepz()
        kernel.cz('QR', 'QL')
        kernel.cz('QL', 'QR')
        kernel.measure()
        program.compile()
        assert program.name == 'symmetric_CZ'

    def test_kernel_name_validation(self):
        program = Program('program_name')
        with pytest.raises(InvalidKernelName):
            # can't be empty
            program.new_kernel("")
        program.new_kernel('valid_name')
        program.new_kernel('validName')
        program.new_kernel('123')
        with pytest.raises(InvalidKernelName):
            # not the second time
            program.new_kernel('valid_name')
        with pytest.raises(InvalidKernelName):
            program.new_kernel('invalid kernel name')
        with pytest.raises(InvalidKernelName):
            program.new_kernel('invalidKernelName!')
        with pytest.raises(InvalidKernelName):
            program.new_kernel('inval.dKernelName')
        assert len(program._kernels) == 3


# noinspection PyProtectedMember
class TestProgramCustomRotations:
    def test_declare_rotations_on_the_fly(self):
        p = Program('RandomRotations')
        k = p.new_kernel('kernel')
        for i in range(31):
            # Microwave rotation on q0
            k.rotate("QL", np.random.rand(), np.random.rand())
            # Microwave rotation on q1
            k.rotate("QR", np.random.rand(), np.random.rand())

        assert len(p._rotations["QL"]) == 31
        assert p.name == 'RandomRotations'
        p.compile()

    def test_declare_rotations_on_the_fly_too_many_raises(self):
        p = Program('RandomRotations')
        k = p.new_kernel('kernel')

        for i in range(31):
            # Microwave rotation on q0
            k.rotate("QL", np.random.rand(), np.random.rand())

        with pytest.raises(HardwareConstraintsError):
            # because a max of 31 rotations per qubit is allowed.
            k.rotate("QL", np.random.rand(), np.random.rand())

        assert len(p._rotations["QL"]) == 31

        # Compilation should work fine as no gates were added
        p.compile()

    def test_adding_same_rotation_does_not_count_double(self):
        p = Program('identical_rotations')
        k = p.new_kernel('kernel')
        for i in range(100):
            # Microwave rotation on q0
            k.rotate("QL", phi=90, theta=180)

        assert len(p._rotations["QL"]) == 1
        assert p.name == 'identical_rotations'
        p.compile()

    def test_mix_of_conventional_and_custom_rotations(self):
        p = Program('RandomRotations')
        k = p.new_kernel('kernel')

        k.gate('rx180', ['QR'])
        k.gate('rx90', ['QR'])
        for i in range(31-2):
            # Microwave rotation on q0
            k.rotate('QR', np.random.rand(), np.random.rand())
        with pytest.raises(HardwareConstraintsError):
            # because a max of 31 rotations per qubit is allowed.
            k.rotate('QR', np.random.rand(), np.random.rand())
        p.compile()


# noinspection PyProtectedMember
class TestSupportedOperations:
    def test_init_all(self):
        p = Program('init_progr')
        k = p.new_kernel('kernel')
        k.prepz()
        last_added = k._gates[-1]
        assert last_added[0] == k.Gates.prepz
        assert set(last_added[1]) == {'QL', 'QR'}
        p.compile()

    def test_measure_all(self):
        p = Program('meas_progr')
        k = p.new_kernel('kernel')
        k.prepz()
        k.measure()
        last_added = k._gates[-1]
        assert last_added[0] == k.Gates.measure
        assert set(last_added[1]) == {'QL', 'QR'}
        p.compile()

    def test_z_rotations(self):
        p = Program('meas_progr')
        k = p.new_kernel('kernel')
        k.prepz()
        k.rz('QL', 23)
        k.measure()
        p.compile()
