"""
This file provides a basic introduction to using the quantum infinity.

Read the comments and try to modify this file to run something interesting.
Have fun!
"""

import qinf  # qinf is a helper library that wraps around the OpenQL compiler
# and provides some extra options to help interact with the hardware
import numpy as np  # basic python libraries can be imported


def tutorial_program(q0: str, q1: str):
    """
    Showcase basic usage of the quantum infinity.

    Parameters
    ----------
    q0 str:
        name of zeroth qubit
    q1 str:
        name of first qubit

    The names of the qubits currently available on the quantum infinity are
        "QL", the left qubit and,
        "QR", the right qubit
    """

    # Create a program
    p = qinf.Program("my_first_program")

    # Add a kernel, by convention each kernel corresponds to a single circuit
    # that starts with preparing the qubits in |0> and ends in a measurement.
    k = p.new_kernel('kernel1')
    # prepz prepares all qubits in |0>
    k.prepz()

    # gates can be applied to a qubit in several different ways.
    # x and y rotations can be performed using the k.rx and k.ry methods.
    k.rx(q0, theta=90)
    k.ry(q0, theta=90)

    # custom euler rotations can be performed using the k.rotate method.
    # phi is the azimutal angle, where 0 corresponds to a rotation about the
    # x-axis and 90 corresponds to a rotation about the y-axis.
    # theta is the angle of rotation.
    k.rotate(q0, phi=0, theta=180)  # <- this is a pi-pulse around the x-axis
    k.cz(q0, q1)  # k.cz performs a conditional phase gate

    # As a compatibility mode we also support some applying some common gates
    # using their name as a reference.
    # The supported common gates are:
    common_gates = ['i', 'rx90', 'rx180', 'ry90', 'ry180', 'rxm90', 'rym90']
    # these gates correspond to rotations around the x and y axis
    # by 90, -90 or 180 degrees. "i" corresponds to the identity.
    for common_name in common_gates:
        k.gate(common_name, [q0])

    # and the qubits are measured using k.measure
    k.measure()

    # to add more datapoints to

    # Let's do a Rabi oscillation on QL
    for i, theta in enumerate(np.linspace(-180, 180, 11)):
        k = p.new_kernel('Rabi_pulse_{}'.format(i))
        k.prepz()
        k.rx(q0, theta=theta)
        k.measure()

    p.compile()
    return p


# the available qubits are QL and QR
tutorial_program(q0='QL', q1='QR')
