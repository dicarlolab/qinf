import json
from os import path


required_fields = [
    "qubits",
    "active_qubits",
    "eqasm_compiler",
    "hardware_settings",
    "resources",
    "topology",
    "gate_decomposition",
    "instructions",
    "simulation_settings"]


def get_config_template(config_file: str):
    """
    Get the config template.

        Parameters
        ----------
        config_file: str
        The name of the config file located inside the templates folder
        Without the .json
        """
    if("../" in config_file):
        raise Exception("Path " + config_file + " not allowed")

    home_path = path.dirname(path.abspath(__file__))
    config_path = path.join(home_path, "templates", config_file+".json")
    with open(path.join(config_path), 'r') as f:
        template = json.load(f)

    if not all(keys in template for keys in required_fields):
        raise Exception(
            "Invalid JSON, make sure the JSON contains all the required fields")

    return template
