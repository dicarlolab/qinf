from types import SimpleNamespace
from os.path import join
import openql.openql as ql
import os
import sys
import re
from enum import Enum, auto
from qinf.ql_config import generate_config, format_rotation
from qinf.ql_config_templates import get_config_template


class HardwareConstraintsError(RuntimeError):
    """This error is raised, if the operation requested violates some
    hardware constraints. It should be raised with a description of a
    constraint, that is violated as a first argument."""
    pass


class InvalidKernelName(RuntimeError):
    """This error is raised, if the name, provided to a Kernel, violates some
    restrictions, i.e., uniqueness or not having special characters."""
    pass


class NoSuchKernel(RuntimeError):
    """This error is raised, if user requested a program for a kernel,
    but it is not present yet."""
    pass


# noinspection PyMethodMayBeStatic,PyUnusedLocal
class Kernel:
    """Wrapper over :class:`openql.openql.Kernel` object.

    Unlike in OpenQL, it is supposed to be instantiated within a
    :func:`qinf.Program.new_kernel` method, since it attempts to do sanity
    checks, while inserting new gates to it.

    Parameters
    ----------
    name: str
        Name of a kernel
    program : qinf.Program
        A program, to that the instance belongs.
    """

    class Gates(Enum):
        measure = auto()
        prepz = auto()
        i = auto()
        rotate = auto()
        cz = auto()

    _ql_add_gate = {
        Gates.measure: '_ql_add_measurement',
        Gates.prepz: '_ql_add_prepz',
        Gates.i: '_ql_add_identity',
        Gates.rotate: '_ql_add_rotate',
        Gates.cz: '_ql_add_cz'
    }

    # If you add a well-known gate, add it also to `Kernel.gate` docstring.
    _well_known = {
        'i': ('idle', tuple()),
        'prepz': ('prepz', tuple()),
        'rx90': ('rx', (90.,)),
        'rx180': ('rx', (180.,)),
        'ry90': ('ry', (90.,)),
        'ry180': ('ry', (180.,)),
        'rxm90': ('rx', (-90,)),
        'rym90': ('ry', (-90,)),
        'cz': ('cz', tuple()),
    }

    def __init__(self, name, program):
        self.name = name
        self._program = program
        self._gates = []

    def measure(self, *qubit_names):
        """Add measurement gates to a set of qubits.

        If no arguments provided, will measure all qubits.

        Parameters
        ----------
        q0, q1,..., qn : str
            Names of qubits to measure.

        Raises
        ------
        qinf.HardwareConstraintsError
            If `qubit_names` provided are not present in the system.
        """
        if len(qubit_names) == 0:
            qubit_names = self._program._layout.active_qubits.keys()
        else:
            self._validate_qubits(*qubit_names)
        self._gates.append((self.Gates.measure, qubit_names, None))

    def prepz(self, *qubit_names):
        """
        Initializes set of qubits in :math:`Z=0` state.

        If no arguments provided, will initialize all qubits.

        Parameters
        ----------
        q0, q1,..., qn : int
            Numbers of qubits to initialize.


        Raises
        ------
        qinf.HardwareConstraintsError
            If `qubit_names` provided are not present in the system.
        """
        if len(qubit_names) == 0:
            qubit_names = self._program._layout.active_qubits.keys()
        else:
            self._validate_qubits(*qubit_names)
        self._gates.append((self.Gates.prepz, qubit_names, None))

    def idle(self, qubit_name):
        """
        Add idling gate to `qubit_name`.

        Parameters
        ----------
        qubit_name : str
            Name of a qubit_name to idle.

        Raises
        ------
        qinf.HardwareConstraintsError
            If `qubit_name` provided is not present in the system.
        """
        self._validate_qubits(qubit_name)
        self._gates.append((self.Gates.i, qubit_name, None))

    def rotate(self, qubit_name, phi, theta):
        """Add a rotation in :math:`xOy` plane, that corresponds to a
        microwave rotation.

        In terms of Euler rotations, this would be expressed as:

        .. math::

            R(\\phi, \\theta) = R_\\text{Euler}(\\phi, \\theta, -\\phi)

        Parameters
        ----------
        qubit_name : str
            Name of a qubit_name to rotate.
        phi : float
            Angle between rotation axis and :math:`Ox` axis in degrees.
        theta : float
            Rotation angle in degrees.

        Raises
        ------
        qinf.HardwareConstraintsError
            If `qubit_name` provided is not present in the system.
        """
        self._validate_qubits(qubit_name)
        self._program._declare_rotation(qubit_name, phi, theta)
        self._gates.append((self.Gates.rotate, qubit_name, (phi, theta)))

    def cz(self, q0_name, q1_name):
        """Adds controlled Z rotation.

        Parameters
        ----------
        q0_name, q1_name : str
            Names of qubits to apply CZ

        Raises
        ------
        qinf.HardwareConstraintsError
            If `q0_name` or `q1_name` provided is not present in the system,
            or `q0_name == q1_name`.
        """
        self._validate_qubits(q0_name, q1_name)
        if q0_name == q1_name:
            raise HardwareConstraintsError(
                'Qubit names `q0_name` and `q1_name` must be different, '
                'got both "{}"'.format(q0_name))
        self._gates.append((self.Gates.cz, (q0_name, q1_name), None))

    def rx(self, qubit, theta=180.):
        """Rotates a qubit along :math:`Ox` axis.

        Parameters
        ----------
        qubit : str
            Name of a qubit to rotate.
        theta : float
            Rotation angle in degrees, defaults to 180.

        Raises
        ------
        qinf.HardwareConstraintsError
            If `qubit_name` provided is not present in the system.
        """
        self.rotate(qubit, 0., theta)

    def ry(self, qubit, theta=180.):
        """Rotates a qubit along :math:`Oy` axis.

        Parameters
        ----------
        qubit : str
            Name of a qubit to rotate.
        theta : float
            Rotation angle in degrees, defaults to 180.

        Raises
        ------
        qinf.HardwareConstraintsError
            If `qubit_name` provided is not present in the system.
        """
        self.rotate(qubit, 90., theta)

    def rz(self, qubit, theta=180.):
        """Rotates a qubit along :math:`Oz` axis.

        Note that this gate gets naively decomposed into microvave rotations
        :func:`qinf.Kernel.rotate` according to:

        .. math::

            R_z(\\phi_z) \\rightarrow R(\\phi=90^\\circ, \\theta=180^\\circ),
            \\ R(\\phi=(\\phi_z - 180^\\circ)/2, \\theta=180^\\circ)

        Parameters
        ----------
        qubit : str
            Name of a qubit to rotate.
        theta : float
            Rotation angle in degrees, defaults to 180.

        Raises
        ------
        qinf.HardwareConstraintsError
            If `qubit_name` provided is not present in the system.
        """
        self.rotate(qubit, phi=90, theta=180)
        self.rotate(qubit, phi=0.5*(theta - 180), theta=180)

    def gate(self, instr, qubit_names):
        """Adds well-known gate by QASM-like instruction.

        Parameters
        ----------
        instr : str
            Instruction name. Currently 'i', 'prepz', 'rx90', 'rx180', 'ry90',
            'ry180', 'rxm90' and 'rym90' are supported.
        qubit_names : list of str
            Names of involved qubits. Pay attention: a list.

        Raises
        ------
        qinf.HardwareConstraintsError
            If `qubit_names` provided are not present in the system.
        """
        method, args = self._well_known[instr]
        getattr(self, method)(*qubit_names, *args)

    def compile(self, platform):
        """Compiles this kernel to :class:`ql.Kernel`. Intended to be called
        by inside a `qinf.Program.compile` method.

        Parameters
        ----------
        platform : openql.openql.Platform
            OpenQL platform to compile for.

        Returns
        -------
        openql.openql.Kernel
            OpenQL kernel for the selected platform

        """
        kernel = ql.Kernel('k_' + self.name, platform,
                           platform.get_qubit_number())
        for gate in self._gates:
            gate_type, qubits, args = gate
            getattr(self, self._ql_add_gate[gate_type])(kernel, qubits, args)
        return kernel

    def _validate_qubits(self, *qubits):
        for q in qubits:
            if q not in self._program._layout.active_qubits.keys():
                raise HardwareConstraintsError(
                    'Qubit "{}" does not exist in '
                    'hardware configuration'.format(q))

    def _ql_add_measurement(self, ql_kernel, qubits, args):
        for q in qubits:
            ql_kernel.measure(self._program.qubit_number(q))

    def _ql_add_prepz(self, ql_kernel, qubits, args):
        for q in qubits:
            ql_kernel.gate('prepz', [self._program.qubit_number(q)])

    def _ql_add_identity(self, ql_kernel, qubit, args):
        ql_kernel.gate('i', [self._program.qubit_number(qubit)])

    def _ql_add_rotate(self, ql_kernel, qubit, args):
        ql_kernel.gate(format_rotation(*args),
                       [self._program.qubit_number(qubit)])

    def _ql_add_cz(self, ql_kernel, qubits, args):
        # FIXME: this is a workaround for a bug in OpenQL
        # Switching qubit indices to match known edge
        if qubits == ("QL", "QR"):
            qubits = ("QR", "QL")
        ql_kernel.gate('cz', [self._program.qubit_number(qubits[0]),
                              self._program.qubit_number(qubits[1])])


class Program:
    """
    Wrapper over :class:`openql.openql.Program` object.

    Parameters
    ----------
    name: str
        Name of the program
    scheduler: str
        name of scheduling algorithm, can be "ALAP" or "ASAP"


    The intended workflow of a Program object is as follows:

    1. Instantiate a program object:

       >>> p = Program('program_name')

    2. Add kernels to the program

       >>> k = p.new_kernel('kernel_name')

    3. Add operations to the kernel e.g.,

       >>> k.prepz()  # initialize all qubits
       >>> k.rx('QL', 90.)  # Rotation around specific axis
       >>> k.rotate('QR', 90., -90.)  # Rotation around free axis
       >>> k.cz('QL', 'QR')  # two-qubit gate
       >>> k.gate('rx180', ['QL'])   # OpenQL-like declaration
       >>> k.measure()  # measure all qubits

    4. compile program

       >>> p.compile()
    """

    def __init__(self, name, scheduler='ALAP'):
        self.name = name
        self.sweep_points = None

        if len(sys.argv) > 1:
            device = sys.argv[1]
            self.config_template = get_config_template(device)
        else:
            self.config_template = get_config_template("CCL")

        active_qubits = self.config_template['active_qubits']
        num_qubits = self.config_template['qubits']
        self._layout = SimpleNamespace(
            num_qubits=num_qubits,
            active_qubits=active_qubits,
            # FIXME: freq groups required for VSM will be supported in future
            # freq_groups=[[0], [2]],
            max_instructions_per_group=31,
        )
        self._kernels = []
        self._scheduler = scheduler
        self._output_dir = ql.get_option('output_dir')
        self.qasm_fp = join(self._output_dir, self.name + '.qasm')
        self.qisa_fp = join(self._output_dir, self.name + '.qisa')
        self._ccl_config_filename = 'ccl_config.json'
        self._mw_config_filename = 'mw_config.json'

        # FIXME: keys should be freq groups
        self._rotations = {
            q_name: set() for q_name in self._layout.active_qubits.keys()}

    @property
    def n_qubits(self):
        return len(self._layout.active_qubits)

    def qubit_number(self, qubit_name):
        """Return qubit number based on a qubit name.

        Parameters
        ----------
        qubit_name : str
            A name of the qubit.

        Raises
        ------
        qinf.HardwareConstraintsError
            If `qubit_name` provided is not present in the system.
        """
        try:
            return self._layout.active_qubits[qubit_name]
        except IndexError:
            raise HardwareConstraintsError(
                "No such qubit: {}".format(qubit_name))

    def new_kernel(self, name):
        """
        Creates a new :class:`qinf.Kernel` for the program.

        Parameters
        ----------
        name : str
            The name of the kernel (case insensitive).
            A kernel name must

            - be unique within a program
            - not contain special characters (dots, colons, semicolons,
              etc.) or spaces.

            Additionally, all kernel names will be prefixed with `k_` to
            prevent any naming conflict with existing instructions.

        Raises
        ------
        qinf.InvalidKernelName
            If `name` provided is invalid.
        """
        self._validate_kernel_name(name)
        kernel = Kernel(name, self)
        self._kernels.append(kernel)
        return kernel

    def get_kernel(self, name):
        for k in self._kernels:
            if k.name == name:
                return k
        raise NoSuchKernel(name)

    def compile(self):
        """
        Compiles the Program.

        Several attributes are added to the :class:`openql.openql.Program`
        object:

        qisa_fp (str):
            filepath of qisa file used for execution.
        qasm_fp (str):
            filepath of qasm file used for simulation.
        ccl_config_fp (str):
            filepath of ccl_config used for exec. and sim.
        mw_config_fp (str):
            filepath of mw_config used to configure mw_awgs.

        """
        if not os.path.exists(self._output_dir):
            os.mkdir(self._output_dir)

        ql.set_option('output_dir', self._output_dir)
        ql.set_option('optimize', 'no')
        ql.set_option('scheduler', self._scheduler)

        # N.B. _get_platform includes config generation
        platform = self._get_platform()
        program = ql.Program(self.name, platform, self._layout.num_qubits)

        for kernel in self._kernels:
            ql_kernel = kernel.compile(platform)
            program.add_kernel(ql_kernel)

        if self.sweep_points:
            program.set_sweep_points(self.sweep_points, len(self.sweep_points))

        program.filename = self.qisa_fp
        return program.compile()

    def _declare_rotation(self, qubit_name, phi, theta):
        """Declare a new rotation, checking hardware constraints."""
        # qubit_idx = self._layout.active_qubits[qubit]
        # group_index = self._qubit_group(qubit)
        r = self._rotations[qubit_name]
        if len(r) >= self._layout.max_instructions_per_group:
            raise HardwareConstraintsError(
                "Number of microwave rotation instructions for qubit {} has "
                "reached hardware limits: it can have {} instructions max."
                .format(qubit_name, self._layout.max_instructions_per_group)
            )
        else:
            r.add((phi, theta))

    def _get_platform(self):
        # Assigning to public attributes as users want to use these
        # after compilation.
        self.ccl_config_fp, self.mw_config_fp = generate_config(
            active_qubits=self._layout.active_qubits,
            rotations_combined=self._rotations,
            mw_pulse_duration=20,
            flux_pulse_duration=40,
            ro_duration=800,
            init_duration=200000,
            output_dir=self._output_dir,
            ccl_config_fn=self._ccl_config_filename,
            mw_config_fn=self._mw_config_filename,
            config_template=self.config_template
        )

        return ql.Platform('QuantumInfinity', self.ccl_config_fp)

    def _validate_kernel_name(self, name):
        """Validates kernel name, calling all known validation routines."""
        self._kname_validate_special_characters(name)
        self._kname_validate_unique(name)

    _kname_validate_special_characters_re = re.compile(r"^[a-zA-Z0-9_]+$")

    @classmethod
    def _kname_validate_special_characters(cls, name):
        if not cls._kname_validate_special_characters_re.match(name):
            raise InvalidKernelName(
                "Invalid kernel name: \"{}\". Kernel name should consist "
                "only of alphanumeric characters and underscores.".format(name)
            )

    def _kname_validate_unique(self, name):
        for k in self._kernels:
            if k.name == name:
                raise InvalidKernelName(
                    "Kernel with a name \"{}\" is already present in the "
                    "program.".format(name)
                )

    @staticmethod
    def _basket_index(item, basket_list):
        for i, basket in enumerate(basket_list):
            if item in basket:
                return i
        raise KeyError('Item is not in baskets at all')
