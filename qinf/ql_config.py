from os.path import join
import json
import numpy as np
from copy import copy
from numpy import pi, sin, cos, exp
from scipy.linalg import expm

_pauli_x = np.array([[0, 1], [1, 0]], dtype=complex)
_pauli_y = np.array([[0, -1j], [1j, 0]], dtype=complex)

_devices = ["CLL", "QCC"]


# noinspection PyTypeChecker
def generate_config(active_qubits: dict,
                    rotations_combined: dict,
                    config_template: dict,
                    mw_pulse_duration: int = 20,
                    flux_pulse_duration: int = 40,
                    ro_duration: int = 800,
                    init_duration: int = 200000,
                    output_dir: str = None,
                    ccl_config_fn: str = 'ccl_config.json',
                    mw_config_fn: str = 'mw_config.json',):
    """
    Generate configuration files to compile and execute an openQL program.

    Parameters
    ----------
    active_qubits: dict
        dict of active qubits with key = qubit_name,
    rotations_combined: dict
        dict of rotations for all qubits: key = qubit_name,
        value = list of rotations. Rotations per qubit are specified as a list
        of tuples (phi, theta), where phi/theta specify euler gate
        parameters in degrees.
    mw_pulse_duration: int
        duration of microwave operations in ns.
    flux_pulse_duration: int
        duration of flux pulses such as CZ in ns.
    ro_duration: int
        duration of measurement operation, including depletion in ns.
    init_duration: int
        duration of qubit initialization by relaxation in ns.
    output_dir: str
        location where the config files are written to. If None uses
        ql.get_output_dir().
    ccl_config_fn: str
        name of the CCL config file.
    mw_config_fn: str
        name of the microwave config file.



    The configuration contains different pieces of information spread over
    different files.
        - CCL_config.json
            - contains
        - mw_config.json
            - contains specification of the microwave rotations for qubits.
    """
    hardware_settings = config_template["hardware_settings"]
    resources = config_template["resources"]
    topology = config_template["topology"]
    gate_decomposition = config_template['gate_decomposition']
    simulation_settings = config_template["simulation_settings"]
    instructions = config_template["instructions"]
    compiler = config_template["eqasm_compiler"]

    ccl_config = {"eqasm_compiler": compiler,
                  "hardware_settings": copy(hardware_settings),
                  "instructions": instructions,
                  "resources": resources,
                  "topology": topology,
                  "gate_decomposition": gate_decomposition,
                  "simulation_settings": simulation_settings}

    mw_config_combined = {}

    # Looping over the individual qubits as each qubit can have individual
    # microwave rotations defined.
    for q_name in active_qubits.keys():
        q_idx = active_qubits[q_name]
        rotations = rotations_combined[q_name]
        # prepz instruction
        ccl_config['instructions'].update(ccl_prepz_instruction(
            target_qubit=q_idx, init_duration=init_duration))
        ccl_config['instructions'].update(ccl_identity_instruction(
            target_qubit=q_idx, mw_pulse_duration=mw_pulse_duration))

        # measurement instruction
        ccl_config['instructions'].update(ccl_measure_instruction(
            target_qubit=q_idx, ro_duration=ro_duration))

        # microwave instructions
        instr_names = named_instructions_from_rotations(
            rotations, target_qubit=q_idx)
        kraus_ops = kraus_operators_from_rotations(rotations)
        # Generate a CCL codeword for every rotation.
        # No conditional gates now.
        for cw, (instr_nm, kraus) in enumerate(zip(instr_names, kraus_ops)):
            ccl_config['instructions'][instr_nm] = ccl_microwave_instruction(
                codeword=cw+1, target_qubit=q_idx,
                kraus_operator=kraus,
                mw_pulse_duration=mw_pulse_duration, condition=None)

        mw_config = rotations_to_mw_config(rotations)
        mw_config_combined[q_name] = mw_config

    # This concept should be removed
    # getting it now from the config, ideally it should come from the layout
    edges = ccl_config['topology']['edges']
    for edge in edges:
        ccl_config['instructions'].update(ccl_flux_instruction(
            base_name='cz',
            codeword=1, q0=edge['src'], q1=edge['dst'],
            flux_pulse_duration=flux_pulse_duration))
        ccl_config['gate_decomposition'].update(ccl_gate_decomposition(
            "cz q" + str(edge['dst']) + " q" + str(edge['src']),
            ["cz q" + str(edge['src']) + " q" + str(edge['dst'])],
        ))
        ccl_config['gate_decomposition'].update(ccl_gate_decomposition(
            "cnot q" + str(edge['dst']) + " q" + str(edge['src']),
            ["cnot q" + str(edge['src']) + " q" + str(edge['dst'])],
        ))
        for cw_flux in range(8):
            ccl_config['instructions'].update(ccl_flux_instruction(
                base_name='fl_cw_{:02}'.format(cw_flux),
                codeword=1, q0=edge['src'], q1=edge['dst'],
                flux_pulse_duration=flux_pulse_duration))

    ccl_config_fp = join(output_dir, ccl_config_fn)
    mw_config_fp = join(output_dir, mw_config_fn)
    with open(ccl_config_fp, 'w') as f:
        json.dump(ccl_config, f, indent=4)
    with open(join(output_dir, mw_config_fn), 'w') as f:
        json.dump(mw_config_combined, f, indent=4)
    return ccl_config_fp, mw_config_fp


def rotations_to_mw_config(rotations):
    """
    Parameters
    ----------
    rotations: list
        Specification of used rotations as a list of tuples (phi, theta) that
        specify euler gate parameters phi and theta in degrees.
    """
    mw_config = {0: {"name": "I", "theta": 0, "phi": 0, "type": "ge"}}
    for i, r in enumerate(rotations):
        # +1 exists because Identity is hardcoded as 0
        mw_config[i+1] = {'phi': r[0], 'theta': r[1], "type": "ge",
                          "name": "rot_{}".format(i+1)}
    return mw_config


def named_instructions_from_rotations(rotations, target_qubit: int):
    """
    Parameters
    ----------
    rotations: list
        Specification of used rotations as a list of tuples (phi, theta) that
        specify euler gate parameters phi and theta in degrees.
    target_qubit: int

    Returns
    -------
    named_instructions: list
    """

    named_instructions = []
    for i, r in enumerate(rotations):
        phi = r[0]
        theta = r[1]
        instr = format_rotation(phi=phi, theta=theta)
        named_instructions.append('{} q{}'.format(
            instr, target_qubit))

    return named_instructions


def kraus_operators_from_rotations(rotations):
    """
    Parameters
    ----------
    rotations: list
        Specification of used rotations as a list of tuples (phi, theta) that
        specify euler gate parameters phi and theta in degrees.

    Returns
    -------
    kraus_operators: list
    """
    return [
        [expm(-0.5j*np.deg2rad(theta) * (_pauli_x*cos(np.deg2rad(phi)) +
                                         _pauli_y*sin(np.deg2rad(phi))))]
        for phi, theta in rotations
    ]


# def generate_resource_description():
#    """Generate resource description. """
#    # N.B. this is currently static, can be improved by using the layout
#    # information.
#    return resources


# def generate_topology_description():
#    """Generate topology description. """
#    # N.B. this is currently static, can be improved by using the layout
#    # information.
#    return topology


def ccl_prepz_instruction(target_qubit: int, init_duration: int):
    prepz = {"prepz q{}".format(target_qubit): {
        "duration": init_duration,
        "latency": 0,
        "qubits": ["q{}".format(target_qubit)],
        "matrix": [[0.0, 1.0], [1.0, 0.0], [1.0, 0.0], [0.0, 0.0]],
        "kraus_repr": None,
        "disable_optimization": True,
        "type": "none",
        "cc_light_instr_type": "single_qubit_gate",
        "cc_light_instr": "prepz",
        "cc_light_codeword": 0,
        "cc_light_opcode": 2}}
    return prepz


def ccl_measure_instruction(target_qubit: int, ro_duration: int):
    measure_instr = {"measure q{}".format(target_qubit): {
        "duration": ro_duration,
        "qubits": ["q{}".format(target_qubit)],
        "matrix": [[0.0, 1.0], [1.0, 0.0], [1.0, 0.0], [0.0, 0.0]],
        "kraus_repr": None,
        "disable_optimization": False,
        "type": "readout",
        "cc_light_instr_type": "single_qubit_gate",
        "cc_light_instr": "measz",
        "cc_light_codeword": 0,
        "cc_light_opcode": 4
    }}
    return measure_instr


def ccl_identity_instruction(target_qubit: int, mw_pulse_duration: int):
    measure_instr = {"i q{}".format(target_qubit): {
        "duration": mw_pulse_duration,
        "qubits": ["q{}".format(target_qubit)],
        "matrix": [[0.0, 1.0], [1.0, 0.0], [1.0, 0.0], [0.0, 0.0]],
        "kraus_repr": [
            [[1.0, 0.0],
             [0.0, 0.0],
             [0.0, 0.0],
             [1.0, 0.0]]],
        "disable_optimization": False,
        "type": "mw",
        "cc_light_instr_type": "single_qubit_gate",
        "cc_light_instr": "cw_00",
        "cc_light_codeword": 0,
        "cc_light_opcode": 8
    }}
    return measure_instr


# noinspection PyUnusedLocal
def ccl_microwave_instruction(
        codeword: int,
        target_qubit: int,
        kraus_operator=None,
        mw_pulse_duration: int = 20,
        condition: str = None) -> dict:
    """
    Creates a CCLight microwave gate instruction.

    Parameters
    ----------
    codeword: int
        integer codeword used to trigger the operation
    target_qubit: int
    kraus_operator: np.array
        kraus operator specifying a noisy version of the gate.
    mw_pulse_duration: int
        duration of the operation in ns
    condition: str
        NotImplemented

    """
    if kraus_operator is not None:
        kraus_repr = kraus_to_json_representation(kraus_operator)
    else:
        kraus_repr = None
    ccl_instr = {
        "duration": mw_pulse_duration,
        "latency": 0,
        "qubits": ["q{}".format(target_qubit)],
        "matrix": [[0.0, 1.0], [1.0, 0.0], [1.0, 0.0], [0.0, 0.0]],
        "kraus_repr": kraus_repr,
        "disable_optimization": False,
        "type": "mw",
                "cc_light_instr_type": "single_qubit_gate",
                "cc_light_instr": "cw_{:02}".format(codeword),
                "cc_light_codeword": codeword,
                "cc_light_opcode": 8+codeword}
    return ccl_instr


def ccl_flux_instruction(base_name: str,
                         codeword: int, q0: int, q1: int,
                         flux_pulse_duration: int):

    ccl_instruction = {"{} q{},q{}".format(base_name, q0, q1): {
        "duration": flux_pulse_duration,
        "latency": 0,
        "qubits": ["q{}".format(q0), "q{}".format(q1)],
        "matrix": [[0.0, 1.0], [1.0, 0.0], [1.0, 0.0], [0.0, 0.0]],
        "kraus_repr": kraus_to_json_representation(kraus_cphase(1.)),
        "disable_optimization": True,
        "type": "flux",
        "cc_light_instr_type": "two_qubit_gate",
        "cc_light_instr": "fl_cw_{:02}".format(codeword),
        "cc_light_right_codeword": codeword,
        "cc_light_left_codeword": codeword,
        "cc_light_opcode": 128+codeword}}
    return ccl_instruction


def ccl_gate_decomposition(alias: str, gates: list):
    return {
        alias: gates
    }


def kraus_to_json_representation(kr_list):
    out = []
    for mat in kr_list:
        re = np.real(mat).reshape((len(mat)*len(mat[0]),))
        im = np.imag(mat).reshape((len(mat)*len(mat[0]),))
        out.append(list(zip(re, im)))
    return out


def kraus_cphase(theta):
    return [np.diag((1., 1., 1., exp(1j*theta*pi)))]


def format_rotation(phi, theta):
    def _angle_std(angle):
        """Format angle in standardized way for instruction."""
        deg = angle
        if abs(int(deg) - deg) < 1e-3:
            out = "{}".format(int(deg))
        else:
            out = "{:.1f}".format(deg)
        return out.replace('-', 'm')

    return 'rot_{}_{}'.format(_angle_std(phi), _angle_std(theta))
